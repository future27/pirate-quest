import { toast } from "react-toastify";

export const dataBlocks = [
  {
    title: "Title block",
    description: "Description block",
  },
];

export const getBlockData = (propTitle) => {
  return dataBlocks.filter((element) => element.title === propTitle);
};

export const notify = (toastText) =>
  toastText ? toast(toastText) : toast("YES! Going next!");

export const thisIsFinish = "Congratulations!";

import React, { useRef, useState, useEffect } from "react";
import LoginForm from "../components/login/LoginForm";
import Footer from "../components/footer/Footer";
import EFFECT from "vanta/dist/vanta.dots.min";
import styleClasses from "./startPage.module.css";

const StartPage = () => {
  const [vantaEffect, setVantaEffect] = useState(null);
  const myRef = useRef(null);
  useEffect(() => {
    if (!vantaEffect) {
      setVantaEffect(
        EFFECT({
          el: myRef.current,
        })
      );
    }
    return () => {
      if (vantaEffect) vantaEffect.destroy();
    };
  }, [vantaEffect]);
  return (
    <div ref={myRef} className={styleClasses.useVanta}>
      <LoginForm />
      <div className={styleClasses.footer__postition}>
        <Footer>Copyright © 2023—2024 «COMMENTATOR»</Footer>
      </div>
    </div>
  );
};
export default StartPage;

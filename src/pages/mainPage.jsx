import React, { useContext, useEffect, useRef, useState } from "react";
import { ToastContainer } from "react-toastify";
import EFFECT from "vanta/dist/vanta.dots.min";
import BlockUI from "../components/block-info/BlockUI";
import PresentBox from "../components/congratulations/PresentBox";
import { AppContext } from "../context/login";
import { dataBlocks } from "../utils";
import styleClasses from "./startPage.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faToolbox } from "@fortawesome/free-solid-svg-icons";
import ModalUI from "../components/modal/ModalUI";

const MainPage = () => {
  const [vantaEffect, setVantaEffect] = useState(null);
  const [modalVis, setModalVis] = useState(true);
  const myRef = useRef(null);
  const { blocksConfirm } = useContext(AppContext);
  useEffect(() => {
    if (!vantaEffect) {
      setVantaEffect(
        EFFECT({
          el: myRef.current,
        })
      );
    }
    return () => {
      if (vantaEffect) vantaEffect.destroy();
    };
  }, [vantaEffect]);

  return (
    <div ref={myRef} className={styleClasses.useVanta}>
      {blocksConfirm !== dataBlocks.length ? (
        <div>
          <ModalUI visible={modalVis} setVisible={setModalVis}>
            <div style={{ color: "#fff" }}>
              <h3>tic-tic</h3>
              <code>Here is some description</code>
            </div>
          </ModalUI>
          <h3 style={{ display: "flex", justifyContent: "center", gap: "5px" }}>
            Here is some tutorial
            <FontAwesomeIcon
              icon={faToolbox}
              bounce
              style={{ color: "#baa78c" }}
            />
          </h3>
          <ToastContainer position="bottom-right" theme="dark" />
          <div className={styleClasses.parent}>
            {dataBlocks.map((element) => (
              <BlockUI
                key={element.title}
                title={element.title}
                description={element.description}
              />
            ))}
          </div>
        </div>
      ) : (
        <div>
          <PresentBox />
        </div>
      )}
    </div>
  );
};
export default MainPage;

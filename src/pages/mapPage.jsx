import React, { useRef, useState, useEffect } from "react";
import EFFECT from "vanta/dist/vanta.dots.min";
import PirateMap from "../assets/map.png";
import styleClasses from "./startPage.module.css";
import CheckMap from "../components/map/CheckMap";
import { useNavigate } from "react-router-dom";

const MapPage = () => {
  const navigate = useNavigate();
  const [vantaEffect, setVantaEffect] = useState(null);
  const myRef = useRef(null);
  useEffect(() => {
    if (!vantaEffect) {
      setVantaEffect(
        EFFECT({
          el: myRef.current,
        })
      );
    }
    return () => {
      if (vantaEffect) vantaEffect.destroy();
    };
  }, [vantaEffect]);
  return (
    <div ref={myRef} className={styleClasses.useVanta}>
      <h3 style={{ display: "flex", justifyContent: "center" }}>
        11.12341, 213.123123
      </h3>
      <CheckMap imageUrl={PirateMap} />
      <div className={styleClasses.descMap}>
        <code>here is some description</code>
      </div>
      <div className={styleClasses.button} onClick={() => navigate("/")}>
        Lets go!
      </div>
    </div>
  );
};

export default MapPage;

import React from "react";
import styleClasses from "./Footer.module.css";

const Footer = ({ children }) => {
  return <div className={styleClasses.footer}>{children}</div>;
};

export default Footer;

import React, { useContext } from "react";
import styleClass from "./LoginForm.module.css";
import { AppContext } from "../../context/login";
import { useNavigate } from "react-router-dom";

const LoginForm = (props) => {
  let navigate = useNavigate();
  const {} = { ...props };
  const { setIsStarted } = useContext(AppContext);

  const LoginSystemUser = (event) => {
    setIsStarted(true);
  };

  const navigateMap = () => {
    navigate("/map");
  };

  return (
    <div className={styleClass.form}>
      <div className={styleClass.title}>COMMENTATOR PRODUCTION</div>
      <div className={styleClass.btns}>
        <div className={styleClass.button} onClick={LoginSystemUser}>
          Start
        </div>
        <div className={styleClass.button} onClick={navigateMap}>
          Check map
        </div>
      </div>
    </div>
  );
};
export default LoginForm;

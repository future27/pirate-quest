import React, { useContext, useEffect, useState } from "react";
import "/node_modules/video-react/dist/video-react.css";
import { useNavigate } from "react-router-dom";
import { Player } from "video-react";
import styleClases from "./PresentBox.module.css";
import ModalUI from "../modal/ModalUI";
import BlockUI from "../block-info/BlockUI";
import CheckMap from "../map/CheckMap";
import dunk_low from "../../assets/prize.jpg";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import Confetti from "react-confetti";
import { thisIsFinish } from "../../utils";
import { AppContext } from "../../context/login";
import video from "../../assets/test2.mp4";

const PresentBox = () => {
  const [showModal, setShowModal] = useState(true);
  const [isShown, setIsShown] = useState(false);
  const navigate = useNavigate();
  const { isStarted, setIsStarted } = useContext(AppContext);

  useEffect(() => {
    setTimeout(() => setIsShown(true));
  }, []);

  const homePageHandler = () => {
    setIsStarted(false);
    navigate("/");
  };

  return (
    <div>
      <ModalUI visible={showModal} setVisible={setShowModal}>
        <div className={styleClases.video__player}>
          <Player playsInline autoPlay src={video} />
        </div>
      </ModalUI>
      {!showModal && (
        <div className={styleClases.container__congratulations}>
          <BlockUI title="Here we go!!!" description={thisIsFinish} />
          <Confetti width={300} height={350} />
          <TransitionGroup>
            <div
              className={
                isShown
                  ? `${styleClases.show} ${styleClases.show__open}`
                  : `${styleClases.show}`
              }
            >
              <CSSTransition timeout={500}>
                <CheckMap imageUrl={dunk_low} sizeW={"350"} sizeH={"350"} />
              </CSSTransition>
              <div className={styleClases.love} onClick={homePageHandler}>
                YEP!
              </div>
            </div>
          </TransitionGroup>
        </div>
      )}
    </div>
  );
};

export default PresentBox;

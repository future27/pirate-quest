import React from "react";
import styleClasses from "./CheckMap.module.css";

const CheckMap = ({ imageUrl, sizeW, sizeH }) => {
  return (
    <div className={styleClasses.pos}>
      <img
        className={styleClasses.picture}
        src={imageUrl}
        alt="alt - image"
        width={sizeW ? sizeW : "450"}
        height={sizeH ? sizeH : "250"}
      />
    </div>
  );
};
export default CheckMap;

import React from "react";

const CheckboxUI = ({ label, isChecked, checkToggle }) => {
  return (
    <div>
      <label>
        <input type="checkbox" checked={isChecked} onChange={checkToggle} />
        <span>{label}</span>
      </label>
    </div>
  );
};
export default CheckboxUI;

import React, { useEffect } from "react";
import classes from "./ModalUI.module.css";

const ModalUI = ({ children, visible, setVisible, referense }) => {
  const rootClasses = [classes.myModal];
  if (visible) {
    rootClasses.push(classes.active);
  }

  const showHandler = () => {
    setVisible(false);
  };
  useEffect(() => {
    setTimeout(() => {
      showHandler();
    }, 36000); // custom timeout
  }, []);
  return (
    <div className={rootClasses.join(" ")} onClick={showHandler}>
      <div
        className={classes.myModalContent}
        onClick={(e) => e.stopPropagation()}
      >
        {children}
      </div>
    </div>
  );
};
export default ModalUI;

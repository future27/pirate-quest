import React, { useState, useContext } from "react";
import styleClasses from "./BlockIU.module.css";
import CheckboxUI from "../UI/Checkbox";
import { AppContext } from "../../context/login";
import { notify } from "../../utils";
import "react-toastify/dist/ReactToastify.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSkullCrossbones } from "@fortawesome/free-solid-svg-icons";

const BlockUI = (props) => {
  const { title, description } = { ...props };
  const [confirm, setConfirm] = useState(false);
  const { blocksConfirm, setBlocksConfirm } = useContext(AppContext);
  
  const checkHandler = (event) => {
    setConfirm(!confirm);
    if (event.target.checked) {
      setBlocksConfirm(blocksConfirm + 1);
      notify();
    } else {
      notify("Упс! Немного поторопились");
      setBlocksConfirm(blocksConfirm - 1);
    }
  };
  return (
    <>
      <div
        className={
          confirm
            ? `${styleClasses.post} ${styleClasses.post__complete}`
            : `${styleClasses.post}`
        }
      >
        <div className={styleClasses.post__content}>
          <strong>{title}</strong>
          <FontAwesomeIcon
            icon={faSkullCrossbones}
            style={{ color: "#baa78c" }}
          />
          <div>{description}</div>
        </div>
        <div className={styleClasses.post__btns}>
          <CheckboxUI isChecked={confirm} checkToggle={checkHandler} />
        </div>
      </div>
    </>
  );
};
export default BlockUI;

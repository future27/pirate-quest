import React, { useState } from "react";
import { AppContext } from "./context/login";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import StartPage from "./pages/startPage";
import MainPage from "./pages/mainPage";
import MapPage from "./pages/mapPage";

const App = () => {
  const [isStarted, setIsStarted] = useState(false);
  const [blocksConfirm, setBlocksConfirm] = useState(0);

  return (
    <AppContext.Provider
      value={{
        isStarted,
        setIsStarted,
        blocksConfirm,
        setBlocksConfirm,
      }}
    >
      <BrowserRouter>
        {isStarted ? (
          <Routes>
            <Route path={"*"} element={<Navigate to="/main" />} />
            <Route path={"/main"} element={<MainPage />} />
          </Routes>
        ) : (
          <Routes>
            <Route path={"/"} element={<StartPage />} />
            <Route path={"*"} element={<Navigate to="/" />} />
            <Route path={"/map"} element={<MapPage />} />
          </Routes>
        )}
      </BrowserRouter>
    </AppContext.Provider>
  );
};

export default App;
